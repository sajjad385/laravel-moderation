<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('post','PostController@create');
Route::post('post','PostController@store');
Route::get('postview','PostController@index');
Route::get('edit/{id}','PostController@edit');
Route::post('edit/{id}','PostController@update');


//web.php

Route::get('create','SearchDataController@create');
Route::post('store','SearchDataController@store');
Route::get('index','SearchDataController@index');
Route::get('search','SearchDataController@result');









Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
